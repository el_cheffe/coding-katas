package kata.romannumbers.pschwarz;

import kata.romannumbers.RomanNumbersConverter;

/**
 * Implementation of {@link RomanNumbersConverter} that works by removing subtractive elements and then just adding everything
 */
public class RomanNumbersConverterImpl implements RomanNumbersConverter {

    @Override
    public int convert(String numeral) {
        long result = 0;
        if (numeral.contains("IV")) {
            result--;
            numeral = numeral.replace("IV", "V");
        }
        if (numeral.contains("IIX")) {
            result--;
            numeral = numeral.replace("IIX", "IX");
        }
        if (numeral.contains("IX")) {
            result--;
            numeral = numeral.replace("IX", "X");
        }
        if (numeral.contains("XL")) {
            result = result - 10;
            numeral = numeral.replace("XL", "L");
        }
        if (numeral.contains("XXC")) {
            result = result - 10;
            numeral = numeral.replace("XXC", "XC");
        }
        if (numeral.contains("XC")) {
            result = result - 10;
            numeral = numeral.replace("XC", "C");
        }
        if (numeral.contains("CD")) {
            result = result - 100;
            numeral = numeral.replace("CD", "D");
        }
        if (numeral.contains("CM")) {
            result = result - 100;
            numeral = numeral.replace("CM", "M");
        }

        long m = numeral.chars().filter(num -> num == 'M').count();
        long d = numeral.chars().filter(num -> num == 'D').count();
        long c = numeral.chars().filter(num -> num == 'C').count();
        long l = numeral.chars().filter(num -> num == 'L').count();
        long x = numeral.chars().filter(num -> num == 'X').count();
        long v = numeral.chars().filter(num -> num == 'V').count();
        long i = numeral.chars().filter(num -> num == 'I').count();

        result = result + (m * 1000) + (d * 500) + (c * 100) + (l * 50) + (x * 10) + (v * 5) + (i * 1);
        return (int)result;

    }

}