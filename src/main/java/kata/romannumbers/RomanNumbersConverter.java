package kata.romannumbers;

/**
 * Converter to find the equivalent arabic number to a given roman number
 */
public interface RomanNumbersConverter {

    /**
     * Converts a given String representing a roman number to its numeric equivalent
     *
     * @param roman the roman number to convert
     * @return the Integer representation of the given number
     * @throws IllegalArgumentException if the given String is not a valid roman number
     */
    int convert(String roman);

}
