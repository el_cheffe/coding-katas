package kata.romannumbers.fpfleiderer;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RomanLiterals {

    private static final Map<Character, RomanLiteral> literals = new HashMap<>();

    static {
        put(new RomanLiteral('I', 1, 'I', 'V', 'X'));
        put(new RomanLiteral('V', 5, 'I'));
        put(new RomanLiteral('X', 10, 'I', 'V', 'X', 'L', 'C'));
        put(new RomanLiteral('L', 50, 'X', 'V', 'I'));
        put(new RomanLiteral('C', 100, 'I', 'V', 'X', 'L', 'C', 'D', 'M'));
        put(new RomanLiteral('D', 500, 'I', 'V', 'X', 'L', 'C'));
        put(new RomanLiteral('M', 1000, 'I', 'V', 'X', 'L', 'C', 'D', 'M'));
    }

    /**
     * Convert a singular (1-letter) roman number to its arabic equivalent
     *
     * @param literal A letter representing a roman number
     * @return the numeric value of the letter
     */
    public static int getValue(char literal) {
        return get(literal).value;
    }

    /**
     * Checks if a given symbol can be a valid successor for the current literal
     *
     * @param literal   the current literal
     * @param successor the successor whose validity is to be checked
     * @return true, if the successor is valid, false otherwise.
     */
    public static boolean isAllowedSuccessor(char literal, char successor) {
        return get(literal).allowedSuccessors.contains(successor);
    }

    private static void put(RomanLiteral literal) {
        literals.put(literal.literal, literal);
    }

    private static RomanLiteral get(Character key) {
        RomanLiteral romanLiteral = literals.get(key);
        if (romanLiteral == null)
            throw new IllegalArgumentException("bad letter '" + key + "'!");
        return romanLiteral;
    }

    private RomanLiterals() {
        // private default constructor for singleton
    }

    static class RomanLiteral {
        private final char literal;
        private final int value;
        private final List<Character> allowedSuccessors;

        RomanLiteral(char literal, int value, Character... allowedSuccessors) {
            this.literal = literal;
            this.value = value;
            this.allowedSuccessors = Arrays.asList(allowedSuccessors);
        }
    }

}