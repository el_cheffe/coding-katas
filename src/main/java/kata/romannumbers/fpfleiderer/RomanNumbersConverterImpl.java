package kata.romannumbers.fpfleiderer;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import kata.romannumbers.RomanNumbersConverter;

/**
 * Implementation of {@link RomanNumbersConverter} that works by splitting the number into blocks,
 * and then calculating and adding each of them.
 */
public class RomanNumbersConverterImpl implements RomanNumbersConverter {

    @Override
    public int convert(String roman) {
        return extractSimpleNumbers(roman).stream()
                .mapToInt(this::calculateFragment)
                .sum();
    }

    /**
     * Split a roman number into n blocks, each representing a part of the number that can then be added up.
     *
     * @param roman the roman number literal
     * @return a list of 'simple' number literals
     */
    private List<String> extractSimpleNumbers(String roman) {
        List<String> simpleNumbers = new ArrayList<>();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < roman.length() - 1; i++) {
            char currentChar = roman.charAt(i);
            char nextChar = roman.charAt(i + 1);
            assertCharactersAreValidCombination(currentChar, nextChar);
            sb.append(currentChar);
            if (blockIsFinished(currentChar, nextChar)) {
                simpleNumbers.add(sb.toString());
                sb = new StringBuilder();
            }
        }
        sb.append(roman.charAt(roman.length() - 1));
        simpleNumbers.add(sb.toString());
        return simpleNumbers;
    }

    /**
     * calculate the value of a simple number block.
     * Blocks can either consist of:
     * <ul>
     * <li>Just one value, repeated an arbitrary number of times.
     * In this case all values should be added.</li>
     * <li>A higher value in the last position (subtractive notation).
     * In this case all other values should be subtracted from the last one.</li>
     * </ul>
     *
     * @param simpleNumber a 'simple' roman number
     * @return the fragment's value
     */
    private int calculateFragment(String simpleNumber) {
        char lowestValueLiteral = simpleNumber.charAt(0);
        char highestValueLiteral = simpleNumber.charAt(simpleNumber.length() - 1);

        int lastLetterValue = RomanLiterals.getValue(highestValueLiteral);
        int sumOfOtherLetterValues = IntStream.range(0, simpleNumber.length() - 1)
                .mapToObj(simpleNumber::charAt)
                .mapToInt(RomanLiterals::getValue)
                .sum();

        if (lowestValueLiteral == highestValueLiteral) {
            return lastLetterValue + sumOfOtherLetterValues;
        } else {
            return lastLetterValue - sumOfOtherLetterValues;
        }
    }

    /**
     * Checks if a number block is finished.
     * A block is finished when the next char has a lesser value than the current char.
     *
     * @param currentChar the current character
     * @param nextChar    the next character
     * @return true, if the block is finished, false otherwise.
     */
    private boolean blockIsFinished(char currentChar, char nextChar) {
        return RomanLiterals.getValue(nextChar) < RomanLiterals.getValue(currentChar);
    }

    /**
     * assert that a given pair of characters is a valid combination, according to syntactical and semantic rules.
     *
     * @param currentChar the current character
     * @param nextChar    the next character
     */
    private void assertCharactersAreValidCombination(char currentChar, char nextChar) {
        if (!RomanLiterals.isAllowedSuccessor(currentChar, nextChar))
            throw new IllegalArgumentException("illegal character combination '" + currentChar + nextChar + "'!");
    }

}