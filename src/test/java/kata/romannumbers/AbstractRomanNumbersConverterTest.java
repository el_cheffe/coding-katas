package kata.romannumbers;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;

import kata.romannumbers.fpfleiderer.RomanNumbersConverterImpl;

public abstract class AbstractRomanNumbersConverterTest {

    private RomanNumbersConverter cut = new RomanNumbersConverterImpl();

    @Before
    public void initializeConverter() {
        cut = getConverter();
    }

    protected abstract RomanNumbersConverter getConverter();

    // kata example tests

    @Test
    public void transform1() {
        assertThat(cut.convert("I")).isEqualTo(1);
    }

    @Test
    public void transform2() {
        assertThat(cut.convert("II")).isEqualTo(2);
    }

    @Test
    public void transform4() {
        assertThat(cut.convert("IV")).isEqualTo(4);
    }

    @Test
    public void transform5() {
        assertThat(cut.convert("V")).isEqualTo(5);
    }

    @Test
    public void transform9() {
        assertThat(cut.convert("IX")).isEqualTo(9);
    }

    @Test
    public void transform42() {
        assertThat(cut.convert("XLII")).isEqualTo(42);
    }

    @Test
    public void transform99() {
        assertThat(cut.convert("XCIX")).isEqualTo(99);
    }

    @Test
    public void transform2013() {
        assertThat(cut.convert("MMXIII")).isEqualTo(2013);
    }

    // exception tests

    @Test(expected = IllegalArgumentException.class)
    public void syntaxError() {
        cut.convert("I X");
    }

    @Test(expected = IllegalArgumentException.class)
    public void semanticError() {
        cut.convert("IC");
    }

    @Test(expected = IllegalArgumentException.class)
    public void transformationError() {
        cut.convert("T");
    }

    // additional transformation tests

    @Test
    public void transform12() {
        assertThat(cut.convert("XII")).isEqualTo(12);
    }

    @Test
    public void transform123() {
        assertThat(cut.convert("XXIII")).isEqualTo(23);
    }

    @Test
    public void transform21() {
        assertThat(cut.convert("XXI")).isEqualTo(21);
    }

    @Test
    public void transform1211() {
        assertThat(cut.convert("MCCXI")).isEqualTo(1211);
    }

    @Test
    public void transform80() {
        //regular variant
        assertThat(cut.convert("LXXX")).isEqualTo(80);
        //tolerated, alternative variant
        assertThat(cut.convert("XXC")).isEqualTo(80);
    }

    @Test
    public void transform1989() {
        //regular variant
        assertThat(cut.convert("MCMLXXXIX")).isEqualTo(1989);
        //tolerated, alternative variant
        assertThat(cut.convert("MCMXXCIX")).isEqualTo(1989);
    }

}
