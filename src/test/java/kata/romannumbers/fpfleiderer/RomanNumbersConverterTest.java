package kata.romannumbers.fpfleiderer;

import kata.romannumbers.AbstractRomanNumbersConverterTest;
import kata.romannumbers.RomanNumbersConverter;

public class RomanNumbersConverterTest extends AbstractRomanNumbersConverterTest {

    @Override
    protected RomanNumbersConverter getConverter() {
        return new RomanNumbersConverterImpl();
    }

}
