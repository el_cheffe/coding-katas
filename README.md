From Roman Numerals
===================

Aufgabenstellung
----------------
Implementierung der Kata „From Roman Numerals“.

siehe http://ccd-school.de/coding-dojo/function-katas/from-roman-numerals/

Vorgehen
--------
1. Neue Packages pro Bearbeiter anlegen
2. Klasse RomanNumbersConverter implementieren
3. Testklasse AbstractRomanNumbersConverterTest extenden und in der abstrakten Methode eine Instanz der Implementierung returnen
